use std::collections::HashMap;
use crate::data1::AssetId;
use crate::spent_lot::SpentLot;
use crate::unspent_lot::UnspentLot;
use crate::Holding;

#[derive(Clone, Debug)]
pub struct Account {
	pub spent_lots: Vec<SpentLot>,
	pub unspent_lots: HashMap<AssetId, Vec<UnspentLot>>,
}

impl Account {
	pub fn holdings(&self) -> &HashMap<AssetId, impl Holding> { &self.unspent_lots }
	pub fn holding(&self, asset: &AssetId) -> Option<&impl Holding> { self.unspent_lots.get(asset) }
}
