use serde::{Serialize, Deserialize};

#[derive(Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum AssetId {
	Usd,
	Symbol(String),
}

impl<T: AsRef<str>> From<T> for AssetId {
	fn from(value: T) -> Self {
		let symbol = value.as_ref();
		AssetId::Symbol(symbol.trim().to_uppercase())
	}
}
