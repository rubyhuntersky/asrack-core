use std::fmt::Debug;
use chrono::{DateTime, Local};
use rust_decimal::Decimal;
use serde::{Serialize, Deserialize};
pub use usd_amount::*;
pub use asset_id::*;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Transaction {
	pub time: DateTime<Local>,
	pub place: String,
	pub detail: TransactionDetail,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub enum TransactionDetail {
	Buy {
		asset_amount: AssetAmount,
		usd_cost: UsdAmount,
	},
	Sell {
		asset_amount: AssetAmount,
		usd_proceeds: UsdAmount,
	},
	// Exchange {
	// 	out_amount: AssetAmount,
	// 	in_amount: AssetAmount,
	// 	usd_value: UsdAmount,
	// },
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct AssetAmount(pub AssetId, pub Decimal);

mod usd_amount;
mod asset_id;
