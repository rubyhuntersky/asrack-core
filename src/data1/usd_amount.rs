use std::ops::{Add, Div, Mul, Sub};
use rust_decimal::Decimal;
use rust_decimal::prelude::Zero;
use serde::{Serialize, Deserialize};

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Serialize, Deserialize)]
pub struct UsdAmount(Decimal);

impl UsdAmount {
	pub fn fraction(&self, numerator: &Decimal, denominator: &Decimal) -> Self {
		let portion = numerator.div(denominator).mul(&self.0);
		UsdAmount(portion.round_dp(2))
	}
	pub fn to_decimal(&self) -> Decimal {
		self.0
	}
}

impl Zero for UsdAmount {
	fn zero() -> Self {
		UsdAmount(Decimal::zero())
	}

	fn is_zero(&self) -> bool {
		self.0.is_zero()
	}
}

impl From<i64> for UsdAmount {
	fn from(value: i64) -> Self {
		UsdAmount(Decimal::from(value).round_dp(2))
	}
}

impl From<&Decimal> for UsdAmount {
	fn from(value: &Decimal) -> Self {
		let value = value.clone();
		Self(value)
	}
}

impl Add for UsdAmount {
	type Output = UsdAmount;
	fn add(self, rhs: Self) -> Self::Output {
		UsdAmount(self.0 + rhs.0)
	}
}

impl Sub for UsdAmount {
	type Output = UsdAmount;
	fn sub(self, rhs: Self) -> Self::Output { UsdAmount(self.0 - rhs.0) }
}

impl Default for UsdAmount {
	fn default() -> Self { UsdAmount::zero() }
}
