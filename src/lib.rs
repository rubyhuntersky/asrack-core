use std::collections::HashMap;
use std::fmt::Debug;
use rust_decimal::Decimal;
use rust_decimal::prelude::Zero;
pub use data1::*;
pub use account::Account;
pub use spent_lot::SpentLot;
pub use unspent_lot::UnspentLot;

pub trait Holding: Debug {
	fn asset(&self) -> &AssetId;
	fn lot_count(&self) -> usize;
	fn total_shares(&self) -> Decimal;
	fn total_cost(&self) -> UsdAmount;
}

pub fn process_transactions(transactions: Vec<Transaction>) -> Account {
	let mut spent_lots: Vec<SpentLot> = Vec::new();
	let mut unspent_lots: HashMap<AssetId, Vec<UnspentLot>> = HashMap::new();
	let transactions = {
		let mut transactions = transactions.to_vec();
		transactions.sort_by_key(|txn| txn.time.clone());
		transactions
	};
	for txn in transactions {
		match txn.detail {
			TransactionDetail::Buy { asset_amount, usd_cost } => {
				let AssetAmount(asset, amount) = &asset_amount;
				let mut lots = unspent_lots.remove(asset).unwrap_or_else(|| Vec::new());
				lots.push(UnspentLot {
					time: txn.time.clone(),
					place: txn.place.clone(),
					asset: asset.clone(),
					original_shares: amount.clone(),
					original_cost: usd_cost.clone(),
					unspent_shares: amount.clone(),
				});
				unspent_lots.insert(asset.clone(), lots);
			}
			TransactionDetail::Sell { asset_amount, usd_proceeds } => {
				let AssetAmount(asset, amount) = &asset_amount;
				let mut to_spend = amount.clone();
				let mut spent_portions = Vec::new();
				let mut new_lots = Vec::new();
				for lot in unspent_lots.remove(asset).unwrap_or(Vec::new()) {
					if to_spend.gt(&Decimal::zero()) {
						let (spent, unspent, more_to_spend) = lot.spend(&to_spend);
						spent_portions.push(spent);
						if let Some(unspent) = unspent {
							new_lots.push(unspent);
						}
						to_spend = more_to_spend;
					} else {
						new_lots.push(lot);
					}
				}
				let overflow_shares = to_spend.gt(&Decimal::zero()).then_some(to_spend);
				if !new_lots.is_empty() {
					unspent_lots.insert(asset.clone(), new_lots);
				}
				if !spent_portions.is_empty() || overflow_shares.is_some() {
					spent_lots.push(SpentLot {
						time: txn.time.clone(),
						place: txn.place.clone(),
						out_asset: asset.clone(),
						out_shares: amount.clone(),
						out_spent_portions: spent_portions,
						out_overflow_shares: overflow_shares,
						in_asset: AssetId::Usd,
						in_shares: usd_proceeds.to_decimal(),
						in_value: usd_proceeds,
					});
				}
			}
			//TransactionDetail::Exchange { .. } => {}
		}
	}
	Account { spent_lots, unspent_lots }
}

#[cfg(test)]
mod tests;
mod data1;
mod spent_lot;
mod account;
mod spent_portion;
mod unspent_lot;
