use std::ops::Sub;
use chrono::{DateTime, Local};
use rust_decimal::Decimal;
use rust_decimal::prelude::Zero;
use crate::data1::{AssetId, UsdAmount};
use crate::spent_portion::SpentPortion;

#[derive(Clone, Debug)]
pub struct SpentLot {
	pub time: DateTime<Local>,
	pub place: String,
	pub out_asset: AssetId,
	pub out_shares: Decimal,
	pub out_spent_portions: Vec<SpentPortion>,
	pub out_overflow_shares: Option<Decimal>,
	pub in_asset: AssetId,
	pub in_shares: Decimal,
	pub in_value: UsdAmount,
}

impl SpentLot {
	pub fn cost(&self) -> UsdAmount {
		self.out_spent_portions.iter().fold(UsdAmount::zero(), |a, b| {
			a + b.cost()
		})
	}
	pub fn gain(&self) -> UsdAmount {
		self.in_value.sub(self.cost())
	}
	pub fn overflow_shares(&self) -> Decimal {
		self.out_overflow_shares.unwrap_or(Decimal::zero())
	}
}
