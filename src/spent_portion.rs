use chrono::{DateTime, Local};
use rust_decimal::Decimal;
use crate::data1::{AssetId, UsdAmount};
use crate::unspent_lot::UnspentLot;

#[derive(Clone, Debug)]
pub struct SpentPortion {
	pub time: DateTime<Local>,
	pub place: String,
	pub asset: AssetId,
	pub original_shares: Decimal,
	pub original_cost: UsdAmount,
	pub spent_shares: Decimal,
}

impl SpentPortion {
	pub fn cost(&self) -> UsdAmount {
		self.original_cost.fraction(&self.spent_shares, &self.original_shares)
	}
	pub fn from_lot(lot: &UnspentLot, spent_shares: &Decimal) -> Self {
		SpentPortion {
			time: lot.time.clone(),
			place: lot.place.clone(),
			asset: lot.asset.clone(),
			original_shares: lot.original_shares.clone(),
			original_cost: lot.original_cost.clone(),
			spent_shares: spent_shares.clone(),
		}
	}
}
