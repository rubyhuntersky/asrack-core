use chrono::{DateTime, Local};
use crate::*;

pub const TSLA: &'static str = "TSLA";

pub fn buy_four_tsla_for_400(time: DateTime<Local>) -> Transaction {
	Transaction {
		time,
		place: "Magic".to_string(),
		detail: TransactionDetail::Buy {
			asset_amount: AssetAmount(TSLA.into(), 4.into()),
			usd_cost: UsdAmount::from(400),
		},
	}
}

pub fn buy_three_tsla_for_300(time: DateTime<Local>) -> Transaction {
	Transaction {
		time,
		place: "Magic".to_string(),
		detail: TransactionDetail::Buy {
			asset_amount: AssetAmount(TSLA.into(), 3.into()),
			usd_cost: UsdAmount::from(300),
		},
	}
}

pub fn buy_one_tsla_for_100(time: DateTime<Local>) -> Transaction {
	Transaction {
		time,
		place: "Magic".to_string(),
		detail: TransactionDetail::Buy {
			asset_amount: AssetAmount(TSLA.into(), 1.into()),
			usd_cost: UsdAmount::from(100),
		},
	}
}

pub fn sell_one_tsla_for_110(time: DateTime<Local>) -> Transaction {
	Transaction {
		time,
		place: "Citadel".to_string(),
		detail: TransactionDetail::Sell {
			asset_amount: AssetAmount(TSLA.into(), 1.into()),
			usd_proceeds: UsdAmount::from(110),
		},
	}
}


pub fn sell_three_tsla_for_330(time: DateTime<Local>) -> Transaction {
	Transaction {
		time,
		place: "Citadel".to_string(),
		detail: TransactionDetail::Sell {
			asset_amount: AssetAmount(TSLA.into(), 3.into()),
			usd_proceeds: UsdAmount::from(330),
		},
	}
}

pub fn sell_four_tsla_for_440(time: DateTime<Local>) -> Transaction {
	Transaction {
		time,
		place: "Citadel".to_string(),
		detail: TransactionDetail::Sell {
			asset_amount: AssetAmount(TSLA.into(), 4.into()),
			usd_proceeds: UsdAmount::from(440),
		},
	}
}
