use chrono::{Duration, Local};
use rust_decimal::Decimal;
use rust_decimal::prelude::{Zero};
use super::{Holding, process_transactions, AssetId, UsdAmount};
use self::fixtures::*;

#[test]
pub fn buy() {
	let t1 = Local::now();
	let account = process_transactions(vec![
		buy_three_tsla_for_300(t1),
	]);

	assert!(account.spent_lots.is_empty());

	let holding = account.holding(&TSLA.into()).unwrap();
	assert_eq!(holding.asset(), &AssetId::from(TSLA));
	assert_eq!(holding.total_shares(), Decimal::from(3));
	assert_eq!(holding.total_cost(), UsdAmount::from(300));
	assert_eq!(holding.lot_count(), 1);
}

#[test]
pub fn sell() {
	let t1 = Local::now();
	let account = process_transactions(vec![
		sell_one_tsla_for_110(t1)
	]);

	let gain = account.spent_lots.first().unwrap();
	assert_eq!(gain.cost(), UsdAmount::zero());
	assert_eq!(gain.gain(), UsdAmount::from(110));
	assert_eq!(gain.overflow_shares(), Decimal::from(1));

	let holding = account.holdings();
	assert!(holding.is_empty());
}

#[test]
pub fn buy_sell_under() {
	let t1 = Local::now();
	let account = process_transactions(vec![
		buy_three_tsla_for_300(t1),
		sell_one_tsla_for_110(t1 + Duration::days(1)),
	]);

	let gain = account.spent_lots.first().unwrap();
	assert_eq!(gain.cost(), UsdAmount::from(100));
	assert_eq!(gain.gain(), UsdAmount::from(10));
	assert_eq!(gain.overflow_shares(), Decimal::zero());

	let holding = account.holding(&TSLA.into()).unwrap();
	assert_eq!(holding.asset(), &AssetId::from(TSLA));
	assert_eq!(holding.total_shares(), Decimal::from(2));
	assert_eq!(holding.total_cost(), UsdAmount::from(200));
	assert_eq!(holding.lot_count(), 1);
}

#[test]
pub fn buy_sell_flat() {
	let t1 = Local::now();
	let account = process_transactions(vec![
		buy_three_tsla_for_300(t1),
		sell_three_tsla_for_330(t1 + Duration::days(1)),
	]);

	let gain = account.spent_lots.first().unwrap();
	assert_eq!(gain.cost(), UsdAmount::from(300));
	assert_eq!(gain.gain(), UsdAmount::from(30));
	assert_eq!(gain.overflow_shares(), Decimal::zero());

	let holdings = account.holdings();
	assert!(holdings.is_empty());
}

#[test]
pub fn buy_sell_over() {
	let t1 = Local::now();
	let account = process_transactions(vec![
		buy_three_tsla_for_300(t1),
		sell_four_tsla_for_440(t1 + Duration::days(1)),
	]);

	let gain = account.spent_lots.first().unwrap();
	assert_eq!(gain.cost(), UsdAmount::from(300));
	assert_eq!(gain.gain(), UsdAmount::from(140));
	assert_eq!(gain.overflow_shares(), Decimal::from(1));

	let holdings = account.holdings();
	assert!(holdings.is_empty());
}

#[test]
pub fn buy_sell3_under() {
	let t1 = Local::now();
	let account = process_transactions(vec![
		buy_four_tsla_for_400(t1),
		sell_one_tsla_for_110(t1 + Duration::days(1)),
		sell_one_tsla_for_110(t1 + Duration::days(2)),
		sell_one_tsla_for_110(t1 + Duration::days(3)),
	]);

	let gain = &account.spent_lots[0];
	assert_eq!(gain.cost(), UsdAmount::from(100));
	assert_eq!(gain.gain(), UsdAmount::from(10));
	assert_eq!(gain.overflow_shares(), Decimal::zero());
	let gain2 = &account.spent_lots[1];
	assert_eq!(gain2.cost(), UsdAmount::from(100));
	assert_eq!(gain2.gain(), UsdAmount::from(10));
	assert_eq!(gain2.overflow_shares(), Decimal::zero());
	let gain2 = &account.spent_lots[1];
	assert_eq!(gain2.cost(), UsdAmount::from(100));
	assert_eq!(gain2.gain(), UsdAmount::from(10));
	assert_eq!(gain2.overflow_shares(), Decimal::zero());
	assert_eq!(account.spent_lots.len(), 3);

	let holding = account.holding(&TSLA.into()).unwrap();
	assert_eq!(holding.asset(), &AssetId::from(TSLA));
	assert_eq!(holding.total_shares(), Decimal::from(1));
	assert_eq!(holding.total_cost(), UsdAmount::from(100));
	assert_eq!(holding.lot_count(), 1);
}

#[test]
pub fn buy3_sell_over() {
	let t1 = Local::now();
	let account = process_transactions(vec![
		buy_one_tsla_for_100(t1),
		buy_one_tsla_for_100(t1 + Duration::days(1)),
		buy_one_tsla_for_100(t1 + Duration::days(2)),
		sell_four_tsla_for_440(t1 + Duration::days(3)),
	]);

	let gain = &account.spent_lots[0];
	assert_eq!(gain.cost(), UsdAmount::from(300));
	assert_eq!(gain.gain(), UsdAmount::from(140));
	assert_eq!(gain.overflow_shares(), Decimal::from(1));
	assert_eq!(account.spent_lots.len(), 1);

	let holdings = account.holdings();
	assert!(holdings.is_empty());
}

mod fixtures;
