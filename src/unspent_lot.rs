use std::fmt::Debug;
use std::ops::Sub;
use chrono::{DateTime, Local};
use rust_decimal::Decimal;
use rust_decimal::prelude::Zero;
use crate::{Holding, AssetId, UsdAmount};
use crate::spent_portion::SpentPortion;


#[derive(Clone, Debug)]
pub struct UnspentLot {
	pub time: DateTime<Local>,
	pub place: String,
	pub asset: AssetId,
	pub original_shares: Decimal,
	pub original_cost: UsdAmount,
	pub unspent_shares: Decimal,
}

impl UnspentLot {
	pub fn cost(&self) -> UsdAmount {
		self.original_cost.fraction(&self.unspent_shares, &self.original_shares)
	}
	pub(crate) fn spend(&self, to_spend: &Decimal) -> (SpentPortion, Option<UnspentLot>, Decimal) {
		assert!(to_spend.gt(&Decimal::zero()));
		assert!(self.unspent_shares.ge(&Decimal::zero()));
		if self.unspent_shares.ge(to_spend) {
			let unspent_overflow = self.unspent_shares.sub(to_spend);
			let spent_portion = SpentPortion::from_lot(&self, to_spend);
			let unspent_lot = if unspent_overflow.gt(&Decimal::zero()) {
				Some(UnspentLot {
					time: self.time.clone(),
					place: self.place.clone(),
					asset: self.asset.clone(),
					original_shares: self.original_shares.clone(),
					original_cost: self.original_cost.clone(),
					unspent_shares: unspent_overflow,
				})
			} else { None };
			(spent_portion, unspent_lot, Decimal::zero())
		} else {
			let unspent_underflow = to_spend.sub(&self.unspent_shares);
			let spent = &self.unspent_shares;
			let spent_portion = SpentPortion::from_lot(&self, spent);
			(spent_portion, None, unspent_underflow)
		}
	}
}

impl Holding for Vec<UnspentLot> {
	fn asset(&self) -> &AssetId {
		&(self.first().unwrap().asset)
	}

	fn lot_count(&self) -> usize {
		self.len()
	}

	fn total_shares(&self) -> Decimal {
		self.iter().fold(Decimal::zero(), |a, b| {
			a + b.unspent_shares
		})
	}
	fn total_cost(&self) -> UsdAmount {
		self.iter().fold(UsdAmount::zero(), |a, b| {
			a + b.cost()
		})
	}
}
