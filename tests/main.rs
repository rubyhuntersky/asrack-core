use std::str::FromStr;
use rust_decimal::Decimal;
use asrack_core::UsdAmount;

#[test]
fn usd_amount_from_decimal() {
	let x = Decimal::from_str("123.45").unwrap();
	let result = UsdAmount::from(&x);
	assert_eq!(result.to_decimal(), x);
}